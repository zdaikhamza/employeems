package com.employeeMS.EmployeeMS.repository;

import com.employeeMS.EmployeeMS.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {

    Employee findEmployeeById(long employeeid);
    Employee findEmployeeByLogin(String login);
    List<Employee> findEmployeeByServiceId(Long seviceId);
}
