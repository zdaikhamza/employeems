package com.employeeMS.EmployeeMS.repository;

import com.employeeMS.EmployeeMS.model.Service;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRepository extends JpaRepository<Service, Long> {
    Service findServiceById(long serviceid);
}
