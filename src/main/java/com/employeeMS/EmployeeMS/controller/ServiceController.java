package com.employeeMS.EmployeeMS.controller;

import com.employeeMS.EmployeeMS.model.Service;
import com.employeeMS.EmployeeMS.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

@RestController
@CrossOrigin
public class ServiceController {

    @Autowired
    ServiceRepository serviceRepository;

    @GetMapping(value = "/services",produces="application/json")
    public List<Service> getAllServices(){
        return serviceRepository.findAll();
    }


    @PostMapping("/service")
    public ResponseEntity<Serializable> createEmployee(HttpServletRequest request, @RequestBody Service service){
        System.out.println("Service"+service.toString());
        serviceRepository.save(service);
        return ResponseEntity.ok().body("Service ajoutee avec succees");
    }

    @GetMapping(value = "/service/{id}",produces = "application/json")
    public ResponseEntity<Service> getServiceById(@PathVariable("id") long serviceid){

        Service service = serviceRepository.findServiceById(serviceid);
        if(service==null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(service);

    }

//    @PutMapping("/service/{id}")
//    public ResponseEntity<Service> updateService(@PathVariable("id") long serviceid,@Valid @RequestBody Service serviceDetails){
//        Service service = serviceRepository.findEmployeeById(serviceid);
//        if(service == null){
//            return ResponseEntity.notFound().build();
//        }
//
//        service.setLibelle(serviceDetails.getLibelle());
//        service.setEmployees(serviceDetails.getEmployees());
//
//        Service updatedservice = serviceRepository.save(service);
//        return ResponseEntity.ok().body(updatedservice);
//    }

    @DeleteMapping("/service/{id}")
    public ResponseEntity<Service> deleteService(@PathVariable("id") long serviceid){
        Service service = serviceRepository.findServiceById(serviceid);
        if(service == null){
            return ResponseEntity.notFound().build();
        }
        serviceRepository.delete(service);

        return ResponseEntity.ok().build();
    }


    @PutMapping("/service/validate")
    public ResponseEntity<Serializable> validateService(@RequestParam("serviceId") long serviceId,@RequestParam("activee") boolean activee){
        Service service = serviceRepository.findServiceById(serviceId);
        service.setActivee(activee);
        serviceRepository.save(service);
        System.out.println("Activee"+activee);
        System.out.println(service.toString());
        if(service.isActivee()) {
            return ResponseEntity.ok().body("Service activee avec succees");
        }
        return ResponseEntity.ok().body("Service desacivee avec succees");

    }


}
