package com.employeeMS.EmployeeMS.controller;

import com.employeeMS.EmployeeMS.model.Employee;
import com.employeeMS.EmployeeMS.repository.EmployeeRepository;
import com.employeeMS.EmployeeMS.repository.ServiceRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/employee")
@CrossOrigin
public class EmployeeController {

        @Autowired
        EmployeeRepository employeeRepository;
        @Autowired
        ServiceRepository serviceRepository;

        @GetMapping(produces="application/json")
        public List<Employee> getAllEmployees(){
            return employeeRepository.findAll();
        }

        @PostMapping
        public ResponseEntity<Serializable> createEmployee(HttpServletRequest request, @RequestBody Employee employee){
            if (employee.getLogin().isEmpty() || employee.getPassword().isEmpty() || employee.getNom().isEmpty() || employee.getPrenom().isEmpty()) {
                System.out.println("Veuillez remplir tous les champs");
                return ResponseEntity.badRequest().body("Veuillez remplir tous les champs");
            }
            Employee emp = employeeRepository.findEmployeeByLogin(employee.getLogin());
            if (emp == null) {
                    employeeRepository.save(employee);
                return ResponseEntity.created(null).body("Utilisateur ajouté");
            }else {
                return ResponseEntity.badRequest().body(employee.getLogin()+" existe déjà ");
            }
        }

        @GetMapping("/{id}")
        public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") long employeeid){

            Employee employee=employeeRepository.findEmployeeById(employeeid);

            if(employee==null) {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok().body(employee);

        }

//        @PutMapping("/{id}")
//        public ResponseEntity<Employee> updateEmployee(@PathVariable("id") long employeeid,@Valid @RequestBody Employee empDetails){
//
//            Employee employee=employeeRepository.findEmployeeById(employeeid);
//            if(employee==null) {
//                return ResponseEntity.notFound().build();
//            }
//            employee.setNom(empDetails.getNom());
//            employee.setPrenom(empDetails.getPrenom());
//            employee.setLogin(empDetails.getLogin());
//            employee.setPassword(empDetails.getPassword());
//            employee.setRole(empDetails.getRole());
//    //        employee.setServices(empDetails.getServices());
//
//            Employee updatedEmployee=employeeRepository.save(employee);
//            return ResponseEntity.ok().body(updatedEmployee);
//        }

        @PutMapping
        public ResponseEntity<Serializable> activateAccount(@RequestParam("serviceId") long serviceId,@RequestParam("userId") long userId,@RequestParam("role") String role) {
            Employee employee = employeeRepository.findEmployeeById(userId);
            employee.setServiceId(serviceId);
            System.out.println(employee.toString());
            employee.setRole(Employee.Role.valueOf(role));
            employeeRepository.save(employee);
            return ResponseEntity.created(null).body("Employee service updatedSuccesfully");
        }

        @DeleteMapping("/{id}")
        public ResponseEntity<Employee> deleteEmployee(@PathVariable("id") long employeeid){

            Employee employee=employeeRepository.findEmployeeById(employeeid);
            if(employee==null) {
                return ResponseEntity.notFound().build();
            }
            employeeRepository.delete(employee);

            return ResponseEntity.ok().build();
        }

        @PostMapping("/login")
        public ResponseEntity<Serializable> login(@RequestParam String login, @RequestParam String password) {
            if (login == null || password == null) {
                return ResponseEntity.badRequest().body("Veuillez saisir votre email et votre mot de passe");
            }
            Employee emp = employeeRepository.findEmployeeByLogin(login);
            if (emp == null || !emp.getPassword().equals(password)) {
                return ResponseEntity.badRequest()
                        .body("Authentification invalide. Veuillez vérifier votre email et votre mot de passe.");
            }
            if (emp.getRole() == null) {
                return ResponseEntity.badRequest()
                        .body("Votre n'est pas encore activee");
            }else if(emp.getRole() != Employee.Role.DIRECTEUR && serviceRepository.findServiceById(emp.getServiceId()).isActivee() == false){
                return ResponseEntity.badRequest()
                        .body("Votre service est inactif");
            }

            emp.setPassword(null);
            emp.setLogin(null);
            emp.setNom(null);
            emp.setPrenom(null);
            return ResponseEntity.ok(emp);
        }

        @GetMapping("/service/{serviceId}")
        public Long[] usersByService(@PathVariable("serviceId") Long serviceId){
            List<Employee> employees = employeeRepository.findEmployeeByServiceId(serviceId);
            Long[] userIds = new Long[employees.size()];
            for(int i=0;i<employees.size(); i++){
                userIds[i] = employees.get(i).getId();
            }
            return userIds;
        }

        @GetMapping("/{userId}/service")
        public long userService(@PathVariable("userId") Long userId){
            Employee emp = employeeRepository.findEmployeeById(userId);
            return emp.getServiceId();
        }

        @GetMapping("/count/{serviceId}")
        public int getServiceEmployeesCount(@PathVariable("serviceId") Long serviceId){
            List<Employee> employees = employeeRepository.findEmployeeByServiceId(serviceId);
            int count = 0;
            for(Employee employee: employees){
                if(employee.getRole() !=null){
                    if(employee.getRole().equals(Employee.Role.EMPLOYEE)){
                        count = count +1;
                    }
                }
            }
            System.out.println("Count of employees of a service" + count);
            return count;
        }



}
