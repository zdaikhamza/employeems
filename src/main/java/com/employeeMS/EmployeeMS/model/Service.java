package com.employeeMS.EmployeeMS.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Data
@Entity
public class Service {

    @Id
    @GeneratedValue
    private long id;
    private String libelle;
    private boolean activee;

//    @OneToMany(targetEntity = Employee.class)
//    private List<Employee> employees;
}
