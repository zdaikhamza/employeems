package com.employeeMS.EmployeeMS.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Getter
@Setter
public class Employee implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String nom;
    private String prenom;
    private String login;
    private String password;
    private Role role;
//    @OneToOne(targetEntity = Service.class)
//    private Service service;

    private Long serviceId;

    public enum Role {
        EMPLOYEE, CHEFSERVICE, DIRECTEUR
    }


}
